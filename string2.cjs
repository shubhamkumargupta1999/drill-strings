function string2(str) {
    let arr = str.split('.');

    if (arr.length === 4) {
        for (let item of arr) {
            if (isNaN(item) === true || (parseInt(item) < 0 || parseInt(item) > 255)) {
                return [];
            }
        }

        let newArr = arr.map((curr) => parseInt(curr));
        return newArr;
    }

    else {
        return [];
    }
}

module.exports = string2;
console.log(string2("111.139.161.143"));