const string1 = require('../string1.cjs');

test('testing string1', () => {
    expect(string1(["$100.45", "$1,002.22", "-$123"])).toStrictEqual([100.45, 1002.22, -123])
})