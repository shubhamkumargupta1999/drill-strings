const string5 = require('../string5.cjs');

test('testing string5', () => {
    expect(string5(["the", "quick", "brown", "fox"])).toBe("the quick brown fox.")
})