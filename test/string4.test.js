const string4 = require('../string4.cjs');

test('testing string4', () => {
    expect(string4({ "first_name": "JoHN", "middle_name": "doe", "last_name": "SMith" })).toBe("John Doe Smith")
})