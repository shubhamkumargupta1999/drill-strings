function string4(obj) {

    newObj = {}

    for (let key in obj) {

        let str = obj[key].toLowerCase();
        let start = str[0].toUpperCase();
        
        let end = str.slice(1, str.length);

        newObj[key] = start + end;
    }

    return Object.values(newObj).join(" ");
}

module.exports = string4;