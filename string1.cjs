function string1(arr) {

    let newArr = arr.map((curr) => {
        newCurr = curr.split('$');
        finalCurr = newCurr.join().replaceAll(',', '');

        if (!isNaN(finalCurr) === true) {
            return parseFloat(finalCurr);
        }

        else {
            return 0;
        }
    })

    return newArr;
}

module.exports = string1;
